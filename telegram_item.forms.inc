<?php

/**
 * Adding an element to send data to state control.
 */
function __telegram_item_process_node_form(&$form) {
	$form['telegram_item'] = array(
		'#type' => 'fieldset',
		'#title' => t('Telegram item'),
		'#group' => 'additional_settings',
		'#weight' => -11,
	);
	
	$form['telegram_item']['telegram_item_post_this_node'] = array(
		'#type' => 'checkbox',
		'#title' => t('Send to Telegram'),
		'#description' => t('After you submit this node it will be send to Telegram.'),
	);
	
	// Getting the settings made in the form of module configuration.
	$apikey = variable_get('telegram_item_apikey');
	$channel = variable_get('telegram_item_channel_name');
	
	if (empty($apikey) && empty($channel)) {
		$form['telegram_item']['telegram_item_post_this_node']['#disabled'] = TRUE;
		$form['telegram_item']['telegram_item_post_this_node']['#value'] = 0;
		$form['telegram_item']['telegram_item_post_this_node']['#description'] =
			t("You can't Telegram nodes until apikey or channel is empty.");
	}
}
